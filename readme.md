# KAFKA CONSUMER WITH ASYNC QUEUE

## Important Configuration

|Variable|Value|Explanation
|---|---|---|
|NODE_ENV|true,false|environment configuration|
|CONSUMER_AUTO_COMMIT, CONSUMER_AUTO_COMMIT_TEST|true, false|Automatically and periodically commit offsets in the background. Note: setting this to false does not prevent the consumer from fetching previously committed start offsets. To circumvent this behaviour set specific start offsets per partition in the call to assign(). |
|CONSUMER_OFFSET, CONSUMER_OFFSET_TEST|smallest, earliest, beginning, largest, latest, end, error|Action to take when there is no initial offset in offset store or the desired offset is out of range: 'smallest','earliest' - automatically reset the offset to the smallest offset, 'largest','latest' - automatically reset the offset to the largest offset, 'error' - trigger an error (ERR__AUTO_OFFSET_RESET) which is retrieved by consuming messages and checking 'message->err'.|
|CONSUME_TIMED, CONSUME_TIMED_TEST|true,false|set consumer to consume per batch|
|CONSUME_INTERVAL, CONSUME_INTERVAL_TEST|number|interval in ms|
|CONSUME_BATCH_SIZE, CONSUME_BATCH_SIZE_TEST|number|data batch size|