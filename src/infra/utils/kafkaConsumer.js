const kafka = require('node-rdkafka');

function createKafkaConsumer(config) {
  return kafka.KafkaConsumer({
    'group.id': config.group,
    'metadata.broker.list': config.broker,
    'enable.auto.commit': config.auto_commit,
  }, {
    'auto.offset.reset': config.offset,
  });
}

module.exports = createKafkaConsumer;
