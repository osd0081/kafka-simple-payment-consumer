/* eslint-disable camelcase */

class MessagesRepositoryMysql {
  constructor(pool) {
    this._pool = pool;
  }

  async UpsertPayment(data, callback) {
    const query = 'INSERT IGNORE INTO payments SET ? ON DUPLICATE KEY UPDATE ?';
    this._pool.query(query, [data, data], (error, results) => callback(error, results));
  }
}

module.exports = MessagesRepositoryMysql;
