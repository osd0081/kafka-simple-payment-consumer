class Payment {
  constructor(payload) {
    const {
      nd,
      ncli,
      periode,
      amount,
      status,
      timestamp,
    } = payload;

    this.nd = nd;
    this.ncli = parseInt(ncli, 10);
    this.periode = periode;
    this.amount = parseFloat(amount);
    this.status = status;
    this.timestamp = new Date(timestamp);
  }
}

module.exports = Payment;
