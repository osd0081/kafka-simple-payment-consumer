const mysql = require('mysql');

const poolMySql = () => {
  if (process.env.NODE_ENV === 'test') {
    return mysql.createPool({
      host: process.env.MYSQLHOST_TEST,
      user: process.env.MYSQLUSER_TEST,
      password: process.env.MYSQLPASSWORD_TEST,
      database: process.env.MYSQLDATABASE_TEST,
    });
  } if (process.env.NODE_ENV === 'prod') {
    return mysql.createPool({
      host: process.env.MYSQLHOST,
      user: process.env.MYSQLUSER,
      password: process.env.MYSQLPASSWORD,
      database: process.env.MYSQLDATABASE,
    });
  }
  const defaultPool = mysql.createPool();
  return defaultPool;
};

module.exports = poolMySql;
