const testConfig = {
  broker: process.env.BROKER_HOST_TEST,
  group: process.env.CONSUMER_GROUP_TEST,
  offset: process.env.CONSUMER_OFFSET_TEST,
  auto_commit: process.env.CONSUMER_AUTO_COMMIT_TEST === 'true',
  topics: [process.env.KAFKA_TOPIC_TEST],
  timed: process.env.CONSUME_TIMED === 'true',
  interval: parseInt(process.env.CONSUME_INTERVAL, 10),
};

const kafkaConsumerConfig = process.env.NODE_ENV === 'test' ? testConfig : {
  broker: process.env.BROKER_HOST,
  group: process.env.CONSUMER_GROUP,
  offset: process.env.CONSUMER_OFFSET,
  auto_commit: process.env.CONSUMER_AUTO_COMMIT === 'true',
  topics: [process.env.KAFKA_TOPIC],
  timed: process.env.CONSUME_TIMED_TEST === 'true',
  interval: parseInt(process.env.CONSUME_INTERVAL_TEST, 10),
};

module.exports = kafkaConsumerConfig;
