const createConsumer = (service, onData) => new Promise((resolve) => {
  service.on('ready', () => {
    console.log('Consumer ready');
    resolve(service);
  }).on('data', onData);
  service.connect();
});

module.exports = createConsumer;
