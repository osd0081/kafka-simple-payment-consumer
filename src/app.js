/* eslint-disable no-console */
require('dotenv').config();
const fs = require('fs');
const async = require('async');

const poolMySql = require('./infra/config/database/mysql/pool');
const kafkaConsumerConfig = require('./infra/config/messaging/kafka/kafkaConsumerConfig');

const Payment = require('./infra/domains/payment');

const createConsumer = require('./infra/messaging/consumer');

const MessagesRepositoryMysql = require('./infra/repository/messagesRepositoryMySQL');

const createKafkaConsumer = require('./infra/utils/kafkaConsumer');
require('dotenv').config();

const init = async () => {
  const kafkaConsumer = createKafkaConsumer(kafkaConsumerConfig);
  const pool = poolMySql();
  const messagesRepositoryMysql = new MessagesRepositoryMysql(pool);

  const databaseLogging = async (err) => {
    fs.writeFile('./log/error-db.txt', err, { flag: 'a+' }, (errWriteErr) => {
      if (errWriteErr) {
        console.log('Error when writing to error-db.txt');
        console.log(errWriteErr, err);
      }
    });
  };

  const queue = async.queue((paymentData, completed) => {
    const remaining = queue.length();
    Promise.resolve(messagesRepositoryMysql.UpsertPayment(paymentData, (error, results) => {
      if (error) {
        databaseLogging(`Fail durring insert ${error}\n${results}`);
        completed(error, { task: null, remaining });
      }
    }));
    completed(null, { task: paymentData, remaining });
  }, 10);

  const consumer = await createConsumer(kafkaConsumer, async ({
    value,
    offset,
    key,
    timestamp,
  }) => {
    consumer.pause(consumer.assignments());

    console.log(`Data ${offset} : ${key} - ${value} - ${timestamp} accepted`);

    const valueData = JSON.parse(value.toString());
    valueData.timestamp = timestamp;
    const paymentData = new Payment(valueData);

    queue.push(paymentData, (error, { task, remaining }) => {
      if (error) {
        consumer.disconnect();
        console.log(`An error occurred while processing task ${task.nd}`);
      } else {
        console.log(`Finished processing task ${task.nd}
               . ${remaining} tasks remaining`);
      }
      if (remaining === 0) {
        consumer.resume(consumer.assignments());
      }
    });
  });

  consumer.subscribe(kafkaConsumerConfig.topics);

  if (!kafkaConsumerConfig.timed) {
    // consume semua paritisi
    consumer.consume();
  } else if (kafkaConsumerConfig.timed) {
    // mode timed
    setInterval(() => {
      consumer.consume(10);
    }, kafkaConsumerConfig.interval);
  }

  // logging all errors

  consumer.on('event.error', (err) => {
    console.log('Error from consumer');
    consumer.unsubscribe();
    consumer.disconnect();

    fs.writeFile('./log/error.txt', err, { flag: 'a+' }, (errWriteErr) => {
      if (errWriteErr) {
        console.log('Error when writing to error.txt');
        console.log(errWriteErr, err);
      }
    });
  });
};

init();
